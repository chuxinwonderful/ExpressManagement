package com.zcx.bean;

import java.sql.Timestamp;

public class User {
    private int id;
    private String name;
    private String phone;
    private String idNumber;//身份证号码
    private String password;
    private String nickname;
    private Timestamp registerTime;//注册时间
    private Timestamp LastLoginTime;//上次登录时间

    public User() {
    }

    public User(String phone) {
        this.phone = phone;
    }

    public User(String name, String phone, String idNumber, String password) {
        this.name = name;
        this.phone = phone;
        this.idNumber = idNumber;
        this.password = password;
    }
    public User(int id, String name, String phone, String idNumber, String password) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.idNumber = idNumber;
        this.password = password;
    }

    public User(int id, String phone, String password, String nickname,
                Timestamp registerTime, Timestamp lastLoginTime) {
        this.id = id;
        this.phone = phone;
        this.password = password;
        this.nickname = nickname;
        this.registerTime = registerTime;
        LastLoginTime = lastLoginTime;
    }

    public User(int id, String name, String phone,
                String idNumber, String password, String nickname,
                Timestamp registerTime, Timestamp lastLoginTime) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.idNumber = idNumber;
        this.password = password;
        this.nickname = nickname;
        this.registerTime = registerTime;
        LastLoginTime = lastLoginTime;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", IdNumber='" + idNumber + '\'' +
                ", password='" + password + '\'' +
                ", nickname='" + nickname + '\'' +
                ", registerTime=" + registerTime +
                ", LastLoginTime=" + LastLoginTime +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Timestamp getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Timestamp registerTime) {
        this.registerTime = registerTime;
    }

    public Timestamp getLastLoginTime() {
        return LastLoginTime;
    }

    public void setLastLoginTime(Timestamp lastLoginTime) {
        LastLoginTime = lastLoginTime;
    }
}
