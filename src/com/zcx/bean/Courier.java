package com.zcx.bean;

import java.sql.Timestamp;

public class Courier {
    private int id;
    private String name;
    private String phone;
    private String idNumber;
    private String password;
    private int sendNumber; //派件数
    private Timestamp registerTime;//注册时间
    private Timestamp LastLoginTime;//上次登录时间

    @Override
    public String toString() {
        return "Courier{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", idNumber='" + idNumber + '\'' +
                ", password='" + password + '\'' +
                ", sendNumber=" + sendNumber +
                ", registerTime=" + registerTime +
                ", LastLoginTime=" + LastLoginTime +
                '}';
    }

    public Courier() {
    }

    public Courier(String name, String phone, String idNumber, String password) {
        this.name = name;
        this.phone = phone;
        this.idNumber = idNumber;
        this.password = password;
       // this.registerTime = registerTime;
    }

    public Courier(int id, String name, String phone, String idNumber,
                   String password, int sendNumber, Timestamp registerTime, Timestamp lastLoginTime) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.idNumber = idNumber;
        this.password = password;
        this.sendNumber = sendNumber;
        this.registerTime = registerTime;
        LastLoginTime = lastLoginTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSendNumber() {
        return sendNumber;
    }

    public void setSendNumber(int sendNumber) {
        this.sendNumber = sendNumber;
    }

    public Timestamp getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Timestamp registerTime) {
        this.registerTime = registerTime;
    }

    public Timestamp getLastLoginTime() {
        return LastLoginTime;
    }

    public void setLastLoginTime(Timestamp lastLoginTime) {
        LastLoginTime = lastLoginTime;
    }
}
