package com.zcx.bean;

public class BootstrapTableCourier {
    private int id;
    private String name;
    private String phone;
    private String idNumber;
    private String password;
    private int sendNumber; //派件数
    private String registerTime;//注册时间
    private String LastLoginTime;//上次登录时间

    public BootstrapTableCourier() {
    }

    public BootstrapTableCourier(int id, String name, String phone, String idNumber, String password,
                                 int sendNumber, String registerTime, String lastLoginTime) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.idNumber = idNumber;
        this.password = password;
        this.sendNumber = sendNumber;
        this.registerTime = registerTime;
        LastLoginTime = lastLoginTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSendNumber() {
        return sendNumber;
    }

    public void setSendNumber(int sendNumber) {
        this.sendNumber = sendNumber;
    }

    public String getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }

    public String getLastLoginTime() {
        return LastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        LastLoginTime = lastLoginTime;
    }
}
