package com.zcx.bean;

public class BootstrapTableUser {
    private int id;
    private String nickname;
    private String phone;
    private String password;
    private String registerTime;//注册时间
    private String LastLoginTime;//上次登录时间

    public BootstrapTableUser() {
    }

    public BootstrapTableUser(int id, String nickname, String phone, String password,
                              String registerTime, String lastLoginTime) {
        this.id = id;
        this.nickname = nickname;
        this.phone = phone;
        this.password = password;
        this.registerTime = registerTime;
        LastLoginTime = lastLoginTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }

    public String getLastLoginTime() {
        return LastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        LastLoginTime = lastLoginTime;
    }
}
