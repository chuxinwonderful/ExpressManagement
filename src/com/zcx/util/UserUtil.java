package com.zcx.util;

import javax.servlet.http.HttpSession;
import java.util.Enumeration;

public class UserUtil {
    public static String getUserName(HttpSession session){
        return (String) session.getAttribute("adminUserName");
    }

    // TODO：还没编写柜子端，没有录入信息
    public static String getUserPhone(HttpSession session){
        return getLoginPhone(session);
    }

    //获得一个code
    public static String getLoginCode(HttpSession session, String userPhone){
        return (String) session.getAttribute(userPhone);
    }

    public static String getLoginPhone(HttpSession session){
        Enumeration<String> attrs = session.getAttributeNames();
        String userPhone = null;
        while (attrs.hasMoreElements()){
            String phone = attrs.nextElement();
            Object code = session.getAttribute(phone);
            if (code == "user" || code == "courier"){
                userPhone = phone;
            }
        }
        return userPhone;
    }

    /**
     * 在发送验证码后将手机号和验证码组成一个键值对存储起来
     * 在点击登录后，将手机号和登录成功与否组成一个键值对存储起来
     * 登录过程中，code=验证码
     * 登录成功，code=courier，登录失败，code=user
     * @param session
     * @param userPhone
     * @param code
     */
    public static void setLoginCode(HttpSession session, String userPhone, String code){
        if (userPhone != null)
        session.setAttribute(userPhone, code);
    }
}
