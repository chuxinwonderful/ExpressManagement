package com.zcx.dao;

import java.util.Date;

public interface AdminDao {
    /**
     * 管理员根据账号密码登录
     * 判断是否登录成功
     * @param userName 账号
     * @param password 密码
     * @return true 表示登录成功
     */

    boolean login(String userName, String password);
    /**
     * 通过admin的用户名
     * 来更新admin的登录时间和登录IP
     * @param userName
     * @param date
     * @param ip
     */
    void updateLoginTime(String userName, Date date, String ip);
}
