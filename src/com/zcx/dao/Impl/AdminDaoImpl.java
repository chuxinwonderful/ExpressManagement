package com.zcx.dao.Impl;

import com.zcx.dao.AdminDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import static com.zcx.util.DruidUtil.close;
import static com.zcx.util.DruidUtil.getConnection;

public class AdminDaoImpl implements AdminDao {
    //验证登录的sql语句
    private static final String SQL_LOGIN =
            "select id from admin where userName=? and password=?";
    //更新登录时间和登录IP的sql语句
    private static final String SQL_UPDATE_LOGIN_TIME =
            "UPDATE admin SET logintime=?,loginip=? WHERE username=? ";

    /**
     * 管理员根据账号密码登录
     * 判断是否登录成功
     *
     * @param userName 账号
     * @param password 密码
     * @return true 表示登录成功
     */
    @Override
    public boolean login(String userName, String password) {
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_LOGIN);
            state.setString(1,userName);
            state.setString(2,password);
            rs = state.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }
        return false;
    }



    /**
     * 通过admin的用户名
     * 来更新admin的登录时间和登录IP
     *
     * @param userName
     * @param date
     * @param ip
     * SQL_UPDATE_LOGIN_TIME = UPDATE admin SET logintime=?,loginip=? WHERE username=?
     */
    @Override
    public void updateLoginTime(String userName, Date date, String ip) {
        Connection conn = null;
        PreparedStatement state = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_UPDATE_LOGIN_TIME);
            //state.setDate(1, (java.sql.Date) date);
            state.setDate(1, new java.sql.Date(date.getTime()));
            state.setString(2,ip);
            state.setString(3,userName);
            state.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(conn, state, null);
        }

    }

}
