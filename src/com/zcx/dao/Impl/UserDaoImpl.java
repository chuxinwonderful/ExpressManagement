package com.zcx.dao.Impl;

import com.zcx.Exception.DuplicateIdNumberException;
import com.zcx.Exception.DuplicatePhoneException;
import com.zcx.bean.User;
import com.zcx.dao.UserDao;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.zcx.util.DruidUtil.close;
import static com.zcx.util.DruidUtil.getConnection;

public class UserDaoImpl implements UserDao {
    //用于查询用户人数和当日注册量
    private String SQL_CONSOLE =
            "SELECT COUNT(id) users_size,COUNT(TO_DAYS(registerTime) = TO_DAYS(NOW()) OR NULL) users_day FROM user";
    //查找所有的用户列表
    private String SQL_FIND_ALL = "SELECT * FROM user";
    //分页查询用户列表
    private String SQL_FIND_LIMIT = "SELECT * FROM user LIMIT ?,?";
    //通过手机号码查找用户
    private String SQL_FIND_BY_PHONE = "SELECT * FROM user WHERE phone=?";
    //通过id查找用户
    private String SQL_FIND_BY_ID = "SELECT * FROM user WHERE id=?";
    //添加用户
    private String SQL_INSERT =
            "INSERT INTO user(name, phone, idNumber, password, registerTime) values(?,?,?,?,NOW())";
    //修改用户
    private String SQL_UPDATE = "UPDATE user SET name=?, phone=?, idNumber=?, password=? WHERE id=?";
    //删除用户
    private String SQL_DELETE = "DELETE FROM user WHERE id=?";
    /**
     * 用于查询数据库中的所有用户信息
     * @return [size:用户数， day:日注册量]
     */
    @Override
    public Map<String, Integer> console() {
        Map<String, Integer> data = new HashMap<>();
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_CONSOLE);
            rs = state.executeQuery();
            while(rs.next()){
                int size = rs.getInt("users_size");
                int day = rs.getInt("users_day");
                data.put("size",size);
                data.put("day",day);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }
        return data;
    }

    /**
     * 用于查询所有用户信息
     * @param limit      是否分页，true分页，false查询所有
     * @param offset     SQL语句中的起始索引
     * @param pageNumber 页查询的数量
     * @return 用户的集合
     */
    @Override
    public List<User> findAll(boolean limit, int offset, int pageNumber) {
        List<User> data = new ArrayList<>();
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            if(limit){
                state = conn.prepareStatement(SQL_FIND_LIMIT);
                state.setInt(1,offset);
                state.setInt(2,pageNumber);
            }else {
                state = conn.prepareStatement(SQL_FIND_ALL);
            }
            rs = state.executeQuery();
            while(rs.next()){
                int id = rs.getInt("id");
                String nickname = rs.getString("nickname");
                String phone = rs.getString("phone");
                String password = rs.getString("password");
                Timestamp registerTime = rs.getTimestamp("registerTime");
                Timestamp lastLoginTime = rs.getTimestamp("LastLoginTime");
                User user = new User(id, phone, password, nickname, registerTime, lastLoginTime);
                data.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }
        return data;
    }

    /**
     * 根据用户手机号码查询用户信息
     * @param phone 用户手机号码
     * @return 查询用户信息，用户不存在时返回null
     */
    @Override
    public User findByPhone(String phone) {
        User user = null;
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_FIND_BY_PHONE);
            state.setString(1,phone);
            rs = state.executeQuery();
            while(rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String idNumber = rs.getString("idNumber");
                String password = rs.getString("password");
                String nickname = rs.getString("nickname");
                Timestamp registerTime = rs.getTimestamp("registerTime");
                Timestamp LastLoginTime = rs.getTimestamp("LastLoginTime");
                user = new User(id, name, phone, idNumber,
                        password, nickname, registerTime, LastLoginTime);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }
        return user;
    }

    /**
     * 根据用户ID查询快递
     * @param id 编号
     * @return 查询用户信息，用户不存在时返回null
     */
    @Override
    public User findById(int id) {
        User user = null;
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_FIND_BY_ID);
            state.setInt(1,id);
            rs = state.executeQuery();
            while(rs.next()){
                String name = rs.getString("name");
                String phone = rs.getString("phone");
                String idNumber = rs.getString("idNumber");
                String password = rs.getString("password");
                String nickname = rs.getString("nickname");
                Timestamp registerTime = rs.getTimestamp("registerTime");
                Timestamp LastLoginTime = rs.getTimestamp("LastLoginTime");
                user = new User(id, name, phone, idNumber,
                        password, nickname, registerTime, LastLoginTime);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }
        return user;
    }

    /**
     * 添加用户
     * @param user 用户对象
     * @return 录入是否成功，true表示成功，false表示失败
     */
    @Override
    public boolean insert(User user) throws DuplicatePhoneException, DuplicateIdNumberException {
        Connection conn = null;
        PreparedStatement state = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_INSERT);
            state.setString(1,user.getName());
            state.setString(2,user.getPhone());
            state.setString(3,user.getIdNumber());
            state.setString(4,user.getPassword());
            int row = state.executeUpdate();
            return row>0;
        } catch (SQLException e) {
            if (e.getMessage().endsWith("for key 'phone'")){
                DuplicatePhoneException dpe = new DuplicatePhoneException("手机号码已存在");
                throw dpe;
            }else if(e.getMessage().endsWith("for key 'idNumber'")){
                DuplicateIdNumberException dne = new DuplicateIdNumberException("身份证号已存在");
                throw dne;
            }else {
                e.printStackTrace();
            }
        } finally {
            close(conn, state, null);
        }
        return false;
    }

    /**
     * 修改用户
     * @param id      要修改的用户id
     * @param newUser 新的用户信息
     * @return 修改是否成功，true表示成功，false表示失败
     */
    @Override
    public boolean update(int id, User newUser) throws DuplicatePhoneException, DuplicateIdNumberException {
        Connection conn = null;
        PreparedStatement state = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_UPDATE);
            state.setString(1,newUser.getName());
            state.setString(2,newUser.getPhone());
            state.setString(3,newUser.getIdNumber());
            state.setString(4,newUser.getPassword());
            state.setInt(5,id);
            int row = state.executeUpdate();
            return row>0;
        } catch (SQLException e) {
            if (e.getMessage().endsWith("for key 'phone'")){
                DuplicatePhoneException dpe = new DuplicatePhoneException("手机号码已存在");
                throw dpe;
            }else if(e.getMessage().endsWith("for key 'idNumber'")){
                DuplicateIdNumberException dne = new DuplicateIdNumberException("身份证号已存在");
                throw dne;
            }else {
                e.printStackTrace();
            }
        } finally {
            close(conn, state, null);
        }
        return false;
    }

    /**
     * 删除用户
     *
     * @param id 要删除的用户id
     * @return 删除是否成功，true表示成功，false表示失败
     */
    @Override
    public boolean delete(int id) {
        Connection conn = null;
        PreparedStatement state = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_DELETE);
            state.setInt(1,id);
            int row = state.executeUpdate();
            return row>0;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(conn, state, null);
        }
        return false;
    }
}
