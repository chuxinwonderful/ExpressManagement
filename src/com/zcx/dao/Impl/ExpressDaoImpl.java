package com.zcx.dao.Impl;

import com.zcx.Exception.DuplicateCodeException;
import com.zcx.bean.Express;
import com.zcx.dao.ExpressDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.*;

import static com.zcx.util.DruidUtil.close;
import static com.zcx.util.DruidUtil.getConnection;

public class ExpressDaoImpl implements ExpressDao {
    //用于查询数据库中的所有快递（含总数和今日新增数），待取快递（含总数和今日数）
    private static final String SQL_CONSOLE =
            "SELECT COUNT(id) total_size, COUNT(TO_DAYS(inTime)=TO_DAYS(NOW()) OR NULL) total_day, COUNT(status=0 OR NULL) not_taken_size, COUNT(TO_DAYS(inTime)=TO_DAYS(NOW()) AND status=0 OR NULL) not_taken_day FROM express ";
    //用于查询所有快递
    private static final String SQL_FIND_ALL = "SELECT * FROM express";
    //用于分页查询快递
    private static final String SQL_FIND_LIMIT = "SELECT * FROM express LIMIT ?,?";
    //根据快递单号查询快递
    private static final String SQL_FIND_BY_NUMBER = "SELECT * FROM express WHERE number=?";
    //根据快递ID查询快递
    private static final String SQL_FIND_BY_ID = "SELECT * FROM express WHERE id=?";
    //根据取件码查询快递
    private static final String SQL_FIND_BY_CODE = "SELECT * FROM express WHERE code=?";
    //根据用户手机号查询他所有的快递
    private static final String SQL_FIND_BY_USER_PHONE = "SELECT * FROM express WHERE userPhone=?";
    //根据录入人的手机号码查询录入的所有记录
    private static final String SQL_FIND_BY_SYS_PHONE = "SELECT * FROM express WHERE sysPhone=?";
    //录入快递
    private static final String SQL_INSERT = "INSERT INTO express(number, username, userPhone, company, code, inTime, status, sysPhone) VALUES(?,?,?,?,?,NOW(),0,?)";
    //修改快递(number,company,username,userPhone,status)
    private static final String SQL_UPDATE = "UPDATE express SET number=?, username=?, company=?, status=? WHERE id=?";
    //取件，修改快递状态
    private static final String SQL_UPDATE_STATUS = "UPDATE express SET status=1,outtime=now(),code=NULL WHERE code=?";
    //删除快递
    private static final String SQL_DELETE = "DELETE FROM express WHERE id=?";

    /**
     * 用于查询数据库中的
     * 所有快递（含总数和今日新增数），
     * 待取快递（含总数和今日数）
     * @return [
     *          {size:总数， day:新增},//所有
     *          {size:总数， day:新增}//待取
     *         ]
     */
    @Override
    public List<Map<String, Integer>> console() {
        List<Map<String, Integer>> data =  new ArrayList<>();
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_CONSOLE);
            rs = state.executeQuery();
            while(rs.next()){
                Integer total_size = rs.getInt("total_size");
                Integer total_day = rs.getInt("total_day");
                Integer not_taken_size = rs.getInt("not_taken_size");
                Integer not_taken_day = rs.getInt("not_taken_day");
                Map<String, Integer> total = new HashMap<>();
                Map<String, Integer> notTaken = new HashMap<>();
                total.put("size", total_size);
                total.put("day", total_day);
                notTaken.put("size", not_taken_size);
                notTaken.put("day", not_taken_day);
                data.add(total);
                data.add(notTaken);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }

        return data;
    }

    /**
     * 用于查询所有快递
     * @param limit 是否分页，true分页，false查询所有
     * @param offset SQL语句中的起始索引
     * @param pageNumber 页查询的数量
     * @return 快递的集合
     */
    @Override
    public List<Express> findAll(boolean limit, int offset, int pageNumber) {
        List<Express> data = new ArrayList<>();
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            if (limit){
                state = conn.prepareStatement(SQL_FIND_LIMIT);
                state.setInt(1,offset);
                state.setInt(2,pageNumber);
            }else {
                state = conn.prepareStatement(SQL_FIND_ALL);
            }
            rs = state.executeQuery();
            while (rs.next()){
                int id = rs.getInt("id");
                String number = rs.getString("number");
                String username = rs.getString("username");
                String userPhone = rs.getString("userPhone");
                String company = rs.getString("company");
                String code = rs.getString("code");
                Timestamp inTime = rs.getTimestamp("inTime");
                Timestamp outTime = rs.getTimestamp("outTime");
                int status = rs.getInt("status");
                String sysPhone = rs.getString("sysPhone");
                Express e = new Express(id, number,username, userPhone, company,
                        code, inTime, outTime, status, sysPhone);
                data.add(e);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }
        return data;
    }

    /**
     * 根据快递单号查询快递
     * @param number 单号
     * @return 查询的快递信息，快递不存在时返回null
     */
    @Override
    public Express findByNumber(String number) {
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_FIND_BY_NUMBER);
            state.setString(1,number);
            rs = state.executeQuery();
            while (rs.next()){
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String userPhone = rs.getString("userPhone");
                String company = rs.getString("company");
                String code = rs.getString("code");
                Timestamp inTime = rs.getTimestamp("inTime");
                Timestamp outTime = rs.getTimestamp("outTime");
                int status = rs.getInt("status");
                String sysPhone = rs.getString("sysPhone");
                Express e = new Express(id, number,username, userPhone, company,
                        code, inTime, outTime, status, sysPhone);
                return e;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }
        return null;
    }

    /**
     * 根据快递编号查询快递
     * @param id 编号
     * @return 查询的快递信息，快递不存在时返回null
     */
    @Override
    public Express findById(int id) {
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_FIND_BY_ID);
            state.setInt(1,id);
            rs = state.executeQuery();
            while (rs.next()){
                String number = rs.getString("number");
                String username = rs.getString("username");
                String userPhone = rs.getString("userPhone");
                String company = rs.getString("company");
                String code = rs.getString("code");
                Timestamp inTime = rs.getTimestamp("inTime");
                Timestamp outTime = rs.getTimestamp("outTime");
                int status = rs.getInt("status");
                String sysPhone = rs.getString("sysPhone");
                Express e = new Express(id, number,username, userPhone, company,
                        code, inTime, outTime, status, sysPhone);
                return e;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }
        return null;
    }

    /**
     * 根据取件码查询快递
     * @param code 取件码
     * @return 查询的快递信息，快递不存在时返回null
     */
    @Override
    public Express findByCode(String code) {
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_FIND_BY_CODE);
            state.setString(1,code);
            rs = state.executeQuery();
            while (rs.next()){
                int id = rs.getInt("id");
                String number = rs.getString("number");
                String username = rs.getString("username");
                String userPhone = rs.getString("userPhone");
                String company = rs.getString("company");
                Timestamp inTime = rs.getTimestamp("inTime");
                Timestamp outTime = rs.getTimestamp("outTime");
                int status = rs.getInt("status");
                String sysPhone = rs.getString("sysPhone");
                Express e = new Express(id, number,username, userPhone, company,
                        code, inTime, outTime, status, sysPhone);
                return e;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }
        return null;
    }

    /**
     * 根据用户手机号查询他所有的快递
     * @param userPhone 用户手机号
     * @return 快递的集合
     */
    @Override
    public List<Express> findByUserPhone(String userPhone) {
        List<Express> data = new ArrayList<>();
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_FIND_BY_USER_PHONE);
            state.setString(1,userPhone);
            rs = state.executeQuery();
            while (rs.next()){
                int id = rs.getInt("id");
                String number = rs.getString("number");
                String username = rs.getString("username");
                String code = rs.getString("code");
                String company = rs.getString("company");
                Timestamp inTime = rs.getTimestamp("inTime");
                Timestamp outTime = rs.getTimestamp("outTime");
                int status = rs.getInt("status");
                String sysPhone = rs.getString("sysPhone");
                Express e = new Express(id, number,username, userPhone, company,
                        code, inTime, outTime, status, sysPhone);
                data.add(e);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }
        return data;
    }

    /**
     * 根据录入人的手机号码查询录入的所有记录
     * @param sysPhone 录入人的手机号码
     * @return 快递的集合
     */
    @Override
    public List<Express> findBySysPhone(String sysPhone) {
        List<Express> data = new ArrayList<>();
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_FIND_BY_SYS_PHONE);
            state.setString(1,sysPhone);
            rs = state.executeQuery();
            while (rs.next()){
                int id = rs.getInt("id");
                String number = rs.getString("number");
                String username = rs.getString("username");
                String userPhone = rs.getString("userPhone");
                String code = rs.getString("code");
                String company = rs.getString("company");
                Timestamp inTime = rs.getTimestamp("inTime");
                Timestamp outTime = rs.getTimestamp("outTime");
                int status = rs.getInt("status");
                Express e = new Express(id, number,username, userPhone, company,
                        code, inTime, outTime, status, sysPhone);
                data.add(e);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }
        return data;
    }

    /**
     * 录入快递
     * @param e 快递对象
     * @return 录入是否成功，true表示成功，false表示失败
     */
    @Override
    public boolean insert(Express e) throws DuplicateCodeException {
        Connection conn = null;
        PreparedStatement state = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_INSERT);
            //INSERT INTO express
            //(number, username, userPhone, company, code, inTime, status, sysPhone)
            //VALUES(?,?,?,?,?,now(),0,?)
            state.setString(1,e.getNumber());
            state.setString(2,e.getUsername());
            state.setString(3,e.getUserPhone());
            state.setString(4,e.getCompany());
            state.setString(5,e.getCode());
            state.setString(6,e.getSysPhone());
            return state.executeUpdate()>0;
        } catch (Exception ex) {
            if(ex.getMessage().endsWith("for key 'code'")){
                DuplicateCodeException dce = new DuplicateCodeException(ex.getMessage());
                throw dce;
            }else{
                ex.printStackTrace();
            }
        } finally {
            close(conn, state, null);
        }
        return false;
    }

    /**
     * 修改快递
     * @param id 要修改的快递id
     * @param newExpress 新的快递的信息（number,company,username,userPhone,status）
     * @return 修改是否成功，true表示成功，false表示失败
     */
    @Override
    public boolean  update(int id, Express newExpress) {
        Connection conn = null;
        PreparedStatement state = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_UPDATE);
            //UPDATE express SET number=?, username=?, company=?, status=? WHERE id=?
            state.setString(1,newExpress.getNumber());
            state.setString(2,newExpress.getUsername());
            state.setString(3,newExpress.getCompany());
            state.setInt(4,newExpress.getStatus());
            state.setInt(5,id);
            return state.executeUpdate()>0;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(conn, state, null);
        }
        return false;
    }

    /**
     * 确认取件，修改快递状态
     * @param code 要修改快递的 取件码
     * @return 修改状态是否成功，true表示成功，false表示失败
     */
    @Override
    public boolean updateStatus(String code) {
        Connection conn = null;
        PreparedStatement state = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_UPDATE_STATUS);
            state.setString(1,code);
            return state.executeUpdate()>0;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(conn, state, null);
        }
        return false;
    }

    /**
     * 删除快递
     * @param id 要删除的快递id
     * @return 删除是否成功，true表示成功，false表示失败
     */
    @Override
    public boolean delete(int id) {
        int row = 0;
        Connection conn = null;
        PreparedStatement state = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_DELETE);
            state.setInt(1,id);
            row = state.executeUpdate();
            return row>0;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(conn, state, null);
        }
        return false;
    }


    private static final String SQL_TOTAL_LAZY_BOARD =
            "SELECT username, count(*) eNum FROM express GROUP BY userPhone ORDER BY eNum DESC LIMIT 0,3";
    /**
     * 懒人总排行榜
     * @return 快递的集合
     */
    public List<Map<String, String>> totalLazyBoard() {
        List<Map<String, String>> data = new ArrayList<>();
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_TOTAL_LAZY_BOARD);
            rs = state.executeQuery();
            while (rs.next()){
                String username = rs.getString("username");
                String eNum = rs.getString("eNum");
                Map<String, String> eData = new HashMap<>();
                eData.put("eNum", eNum);
                eData.put("username", username);
                data.add(eData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }
        return data;
    }

    private static final String SQL_YEAR_LAZY_BOARD =
            "SELECT username, count(*) eNum FROM express where YEAR(intime) = YEAR(NOW()) GROUP BY userPhone ORDER BY eNum DESC LIMIT 0,3";

    @Override
    public List<Map<String, String>> yearLazyBoard() {
        List<Map<String, String>> data = new ArrayList<>();
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_YEAR_LAZY_BOARD);
            rs = state.executeQuery();
            while (rs.next()){
                String username = rs.getString("username");
                String eNum = rs.getString("eNum");
                Map<String, String> eData = new HashMap<>();
                eData.put("eNum", eNum);
                eData.put("username", username);
                data.add(eData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }
        return data;
    }

    private static final String SQL_MONTH_LAZY_BOARD =
            "SELECT username, count(*) eNum FROM express WHERE YEAR(intime) = YEAR(NOW()) AND MONTH(intime)=MONTH(NOW()) GROUP BY userPhone ORDER BY eNum DESC LIMIT 0,3";
    @Override
    public List<Map<String, String>> monthLazyBoard() {
        List<Map<String, String>> data = new ArrayList<>();
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_MONTH_LAZY_BOARD);
            rs = state.executeQuery();
            while (rs.next()){
                String username = rs.getString("username");
                String eNum = rs.getString("eNum");
                Map<String, String> eData = new HashMap<>();
                eData.put("eNum", eNum);
                eData.put("username", username);
                data.add(eData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }
        return data;
    }


}
