package com.zcx.dao.Impl;

import com.zcx.Exception.DuplicateIdNumberException;
import com.zcx.Exception.DuplicatePhoneException;
import com.zcx.bean.Courier;
import com.zcx.dao.CourierDao;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.zcx.util.DruidUtil.close;
import static com.zcx.util.DruidUtil.getConnection;

public class CourierDaoImpl implements CourierDao {
    //用于查询快递员人数和当日注册量
    private static final String SQL_CONSOLE =
            "SELECT COUNT(id) courier_size,COUNT(TO_DAYS(registerTime)=TO_DAYS(NOW()) OR NULL) courier_day FROM courier";
    //查找所有的快递员列表
    private static final String SQL_FIND_ALL = "SELECT * FROM courier";
    //分页查询快递员列表
    private static final String SQL_FIND_LIMIT = "SELECT * FROM courier LIMIT ?,?";
    //通过手机号码查找快递员
    private static final String SQL_FIND_BY_PHONE = "SELECT * FROM courier WHERE phone=?";
    //添加快递员
    private static final String SQL_INSERT =
            "INSERT INTO courier VALUES(null,?,?,?,?,0,NOW(),null)";
    //修改快递员
    private static final String SQL_UPDATE =
            "UPDATE courier SET name=?,phone=?,idNumber=?,password=? WHERE id=?";
    //删除快递员
    private static final String SQL_DELETE = "DELETE FROM courier WHERE id=?";
    /**
     * 用于查询数据库中的所有快递员信息
     *
     * @return [size:快递员数， day:快递员日注册量]
     */
    @Override
    public Map<String, Integer> console() {
        Map<String, Integer> data = new HashMap<>();
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_CONSOLE);
            rs = state.executeQuery();
            while(rs.next()){
                int courier_size = rs.getInt("courier_size");
                int courier_day = rs.getInt("courier_day");
                data.put("size",courier_size);
                data.put("day",courier_day);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }
        return data;
    }

    /**
     * 用于查询所有快递员信息
     * @param limit      是否分页，true分页，false查询所有
     * @param offset     SQL语句中的起始索引
     * @param pageNumber 页查询的数量
     * @return 快递员的集合
     */
    @Override
    public List<Courier> findAll(boolean limit, int offset, int pageNumber) {
        List<Courier> data = new ArrayList<>();
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            if(limit){
                state = conn.prepareStatement(SQL_FIND_LIMIT);
                state.setInt(1,offset);
                state.setInt(2,pageNumber);
            }else{
                state = conn.prepareStatement(SQL_FIND_ALL);
            }
            rs = state.executeQuery();
            while(rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String phone = rs.getString("phone");
                String idNumber = rs.getString("idNumber");
                String password = rs.getString("password");
                int sendNumber = rs.getInt("sendNumber");
                Timestamp registerTime = rs.getTimestamp("registerTime");
                Timestamp lastLoginTime = rs.getTimestamp("LastLoginTime");
                Courier courier = new Courier(id, name, phone, idNumber, password, sendNumber, registerTime, lastLoginTime);
                data.add(courier);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }
        return data;
    }

    /**
     * 根据快递员手机号码查询快递员信息
     * @param phone 快递员手机号码
     * @return 查询快递员信息，快递员不存在时返回null
     */
    @Override
    public Courier findByPhone(String phone) {
        Courier courier = null;
        Connection conn = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_FIND_BY_PHONE);
            state.setString(1,phone);
            rs = state.executeQuery();
            while(rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String idNumber = rs.getString("idNumber");
                String password = rs.getString("password");
                int sendNumber = rs.getInt("sendNumber");
                Timestamp registerTime = rs.getTimestamp("registerTime");
                Timestamp lastLoginTime = rs.getTimestamp("LastLoginTime");
                courier = new Courier(id, name, phone, idNumber, password, sendNumber, registerTime, lastLoginTime);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(conn, state, rs);
        }

        return courier;
    }

    /**
     * 添加快递员
     * @param courier 快递员对象
     * @return 录入是否成功，true表示成功，false表示失败
     */
    @Override
    public boolean insert(Courier courier) throws DuplicatePhoneException, DuplicateIdNumberException {
        Connection conn = null;
        PreparedStatement state = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_INSERT);
            state.setString(1,courier.getName());
            state.setString(2,courier.getPhone());
            state.setString(3,courier.getIdNumber());
            state.setString(4,courier.getPassword());
            int row = state.executeUpdate();
            return row>0;
        } catch (SQLException e) {
            if (e.getMessage().endsWith("for key 'phone'")){
                DuplicatePhoneException dpe = new DuplicatePhoneException("手机号码已存在");
                throw dpe;
            }else if(e.getMessage().endsWith("for key 'idNumber'")){
                DuplicateIdNumberException dne = new DuplicateIdNumberException("身份证号已存在");
                throw dne;
            }else {
                e.printStackTrace();
            }
        } finally {
            close(conn, state, null);
        }
        return false;
    }

    /**
     * 修改快递
     * @param id      要修改的快递员id
     * @param newCourier 新的快递员信息
     * @return 修改是否成功，true表示成功，false表示失败
     */
    @Override
    public boolean update(int id, Courier newCourier) throws DuplicatePhoneException, DuplicateIdNumberException {
        Connection conn = null;
        PreparedStatement state = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_UPDATE);
            state.setString(1,newCourier.getName());
            state.setString(2,newCourier.getPhone());
            state.setString(3,newCourier.getIdNumber());
            state.setString(4,newCourier.getPassword());
            state.setInt(5,id);
            int row = state.executeUpdate();
            return row>0;
        } catch (SQLException e) {
            if (e.getMessage().endsWith("for key 'phone'")){
                DuplicatePhoneException dpe = new DuplicatePhoneException("手机号码已存在");
                throw dpe;
            }else if(e.getMessage().endsWith("for key 'idNumber'")){
                DuplicateIdNumberException dne = new DuplicateIdNumberException("身份证号已存在");
                throw dne;
            }else {
                e.printStackTrace();
            }
        } finally {
            close(conn, state, null);
        }
        return false;
    }

    /**
     * 删除快递员
     * @param id 要删除的快递员id
     * @return 删除是否成功，true表示成功，false表示失败
     */
    @Override
    public boolean delete(int id) {
        Connection conn = null;
        PreparedStatement state = null;
        try {
            conn = getConnection();
            state = conn.prepareStatement(SQL_DELETE);
            state.setInt(1,id);
            int row = state.executeUpdate();
            return row>0;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(conn, state, null);
        }
        return false;
    }
}
