package com.zcx.dao;

import com.zcx.Exception.DuplicateCodeException;
import com.zcx.bean.Express;

import java.util.List;
import java.util.Map;

public interface ExpressDao {
    /**
     * 用于查询数据库中的所有快递（含总数和今日新增数），
     * 待取快递（含总数和今日数）
     * @return [
     *          {size:总数， day:新增},//所有
     *          {size:总数， day:新增}//待取
     *         ]
     */
    List<Map<String,Integer>> console();

    /**
     * 用于查询所有快递
     * @param limit 是否分页，true分页，false查询所有
     * @param offset SQL语句中的起始索引
     * @param pageNumber 页查询的数量
     * @return 快递的集合
     */
    List<Express> findAll(boolean limit, int offset, int pageNumber);

    /**
     * 根据快递单号查询快递
     * @param number 单号
     * @return 查询的快递信息，快递不存在时返回null
     */
    Express findByNumber(String number);

    /**
     * 根据快递ID查询快递
     * @param id 编号
     * @return 查询的快递信息，快递不存在时返回null
     */
    Express findById(int id);

    /**
     * 根据取件码查询快递
     * @param code 取件码
     * @return 查询的快递信息，快递不存在时返回null
     */
    Express findByCode(String code);

    /**
     * 根据用户手机号查询他所有的快递
     * @param userPhone 用户手机号
     * @return 快递的集合
     */
    List<Express> findByUserPhone(String userPhone);

    /**
     * 根据录入人的手机号码查询录入的所有记录
     * @param sysPhone 录入人的手机号码
     * @return 快递的集合
     */
    List<Express> findBySysPhone(String sysPhone);

    /**
     * 录入快递
     * @param e 快递对象
     * @return 录入是否成功，true表示成功，false表示失败
     */
    boolean insert(Express e) throws DuplicateCodeException;

    /**
     * 修改快递
     * @param id 要修改的快递id
     * @param newExpress 新的快递的信息（number,company,username,userPhone,status）
     * @return 修改是否成功，true表示成功，false表示失败
     */
    boolean update(int id, Express newExpress);

    /**
     * 确认取件，修改快递状态
     * @param code 要修改的快递取件码
     * @return 修改状态是否成功，true表示成功，false表示失败
     */
    boolean updateStatus(String code);

    /**
     * 删除快递
     * @param id 要删除的快递id
     * @return 删除是否成功，true表示成功，false表示失败
     */
    boolean delete(int id);

    List<Map<String, String>> totalLazyBoard();
    List<Map<String, String>> yearLazyBoard();
    List<Map<String, String>> monthLazyBoard();
}
