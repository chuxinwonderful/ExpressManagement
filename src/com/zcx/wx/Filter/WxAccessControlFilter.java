package com.zcx.wx.Filter;

import com.zcx.util.UserUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter({"/index.html","/addExpress.html","/expressAssist.html",
            "/expressList.html","/lazyboard.html","/personQRcode.html",
        "/pickExpress.html","/updatePass.html","/userCheckStart.html",
        "/wxIdCardUserInfoModify.html","/wxIdCardUserInfoModify.html"})
public class WxAccessControlFilter implements javax.servlet.Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        System.out.println("WxAccessControlFilterUserUtil->loginPhone:  "+UserUtil.getLoginPhone(request.getSession()));
        if (UserUtil.getLoginPhone(request.getSession()) != null){
            chain.doFilter(req, resp);
        }else{
            response.sendRedirect("/login.html");
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
