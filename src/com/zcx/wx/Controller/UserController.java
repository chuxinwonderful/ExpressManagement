package com.zcx.wx.Controller;

import com.zcx.bean.Courier;
import com.zcx.bean.Message;
import com.zcx.bean.User;
import com.zcx.mvc.ResponseBody;
import com.zcx.service.Impl.CourierServiceImpl;
import com.zcx.service.Impl.UserServiceImpl;
import com.zcx.util.JSONUtil;
import com.zcx.util.RandomUtil;
import com.zcx.util.UserUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//我爱胡嘉祥
public class UserController {
    String userPhone = null;
    String code = null;
    User user = null;
    Courier courier = null;

    @ResponseBody("/wx/autoLogin.do")
    public String autoLogin(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        String loginPhone = UserUtil.getLoginPhone(session);
        int flag = -1;
        if(session.getAttribute("isLogin") != null) {
            flag = (int) session.getAttribute("isLogin");
        }
        Message msg = new Message(-1);
        if (loginPhone != null && flag == 0){
            msg.setStatus(0);
            session.setAttribute("isLogin", true);
        }else{
            msg.setResult("输入手机号码开始登陆");
        }
        String json = JSONUtil.toJSON(msg);
        System.out.println(json);
        return json;
    }
    @ResponseBody("/wx/loginSMS.do")
    public String loginSMS(HttpServletRequest request, HttpServletResponse response) {
        userPhone = request.getParameter("userPhone");
        code = RandomUtil.getCode() + "";
        //boolean flag = SMSUtil.loginSMS(userPhone, code);
        boolean flag = true;
        Message msg = new Message();
        if (flag) {
            msg.setStatus(0);
            msg.setResult("验证码已发送，请注意查收");
            UserUtil.setLoginCode(request.getSession(), userPhone, code);
        } else {
            msg.setStatus(-1);
            msg.setResult("验证码发送失败，请检查手机号码或稍后再试");
        }
        String json = JSONUtil.toJSON(msg);
        System.out.println("UserController->code:"+code);
        return json;
    }

    @ResponseBody("/wx/login.do")
    public String login(HttpServletRequest request, HttpServletResponse response) {
        userPhone = request.getParameter("userPhone");
        String userCode = request.getParameter("code");
        String sysCode = code;
        Message msg = new Message();
//        if (sysCode == null) {
//            msg.setStatus(-1);
//            msg.setResult("登录失败，手机号码未获取短信");
//        }else if (userCode.equals(sysCode)){
            if (true) {
            //验证码正确，验证身份
            user = new UserServiceImpl().findByPhone(userPhone);
            courier = new CourierServiceImpl().findByPhone(userPhone);
            if (courier != null) {
                //快递员登录
                msg.setStatus(0);
                code = "courier";
            } else if (user != null) {
                //用户登录
                msg.setStatus(1);
                code = "user";
            } else {
                msg.setStatus(1);
                code = "user";
                //注册,只有手机号的用户信息
                user = new User(userPhone);
                //将用户信息写入数据库
                new UserServiceImpl().insert(user);
            }
            UserUtil.setLoginCode(request.getSession(), userPhone, code);
            request.getSession().setAttribute("isLogin",0);
        } else {
            msg.setStatus(-2);
            msg.setResult("登录失败，验证码错误");
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseBody("/wx/loginInfo.do")
    public String loginInfo(HttpServletRequest request, HttpServletResponse response) {
        code = UserUtil.getLoginCode(request.getSession(), userPhone);
        UserUtil.setLoginCode(request.getSession(), userPhone, code);
        Message msg = new Message();
        if ("courier".equals(code)) {
            msg.setStatus(0);
            if (courier.getName() == null) {
                msg.setResult("欢迎您，快递员：" + userPhone
                );
            } else {
                msg.setResult("欢迎您，快递员：" + courier.getName());
            }
        } else if ("user".equals(code)) {
            msg.setStatus(1);
            if (user.getName() == null){
                msg.setResult("欢迎您，用户：" + userPhone);
            }else {
                msg.setResult("欢迎您，用户：" + user.getName());
            }
        } else {
            msg.setStatus(1);
            msg.setResult("欢迎您，" + userPhone + "用户");
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseBody("/wx/logout.do")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        //request.getSession().invalidate();
        request.getSession().setAttribute("isLogin",-1);
        Message msg = new Message(0);
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseBody("/wx/userHome.do")
    public String userHome(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        String loginPhone = UserUtil.getLoginPhone(session);
        String loginCode = UserUtil.getLoginCode(session, loginPhone);
        Courier courier = null;
        User user = null;
        Message msg = new Message(0);
        if(loginCode == "courier"){
            courier = new CourierServiceImpl().findByPhone(loginPhone);
            msg.setResult(courier.getName());
        }else if (loginCode == "user"){
            user = new UserServiceImpl().findByPhone(loginPhone);
            msg.setResult(user.getNickname());
        }else{
            msg.setStatus(-1);
            msg.setResult("未登录");
        }
        String json = JSONUtil.toJSON(msg);
        System.out.println("UserController->json");
        System.out.println(json);
        return json;
    }
}
