package com.zcx.wx.Controller;

import com.zcx.bean.BootstrapTableExpress;
import com.zcx.bean.Express;
import com.zcx.bean.Message;
import com.zcx.mvc.ResponseBody;
import com.zcx.mvc.ResponseView;
import com.zcx.service.Impl.ExpressServiceImpl;
import com.zcx.util.DateFormatUtil;
import com.zcx.util.JSONUtil;
import com.zcx.util.UserUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class QRCodeController {
    @ResponseView("/wx/createQRCode.do")
    public String createQRCode(HttpServletRequest request, HttpServletResponse response) {
        String type = request.getParameter("type");

        String qRCode = null;
        if ("express".equals(type)) {
            String code = request.getParameter("code");
            qRCode = "express_" + code;
        } else {
            String userPhone = UserUtil.getLoginPhone(request.getSession());
            qRCode = "userPhone_" + userPhone;
        }
        request.getSession().setAttribute("qrcode", qRCode);
        return "/personQRcode.html";
    }

    @ResponseBody("/wx/getQRCode.do")
    public String getQRCode(HttpServletRequest request, HttpServletResponse response) {
        String qrcode = (String) request.getSession().getAttribute("qrcode");
        Message msg = new Message();
        if (qrcode == null) {
            msg.setStatus(-1);
            msg.setResult("获取二维码失败，请重试！");
        } else {
            msg.setStatus(0);
            msg.setResult(qrcode);
        }
        return JSONUtil.toJSON(msg);
    }

    @ResponseBody("/wx/updateStatus.do")
    public String updateStatus(HttpServletRequest request, HttpServletResponse response) {
        String code = request.getParameter("code");
        boolean flag = new ExpressServiceImpl().updateStatus(code);
        Message msg = new Message();
        if (flag) {
            msg.setStatus(0);
            msg.setResult("取件成功");
        } else {
            msg.setStatus(-1);
            msg.setResult("取件失败");
        }
        System.out.println("QRCodeController->updateStatus.do");
        System.out.println("code："+code);
        System.out.println("json"+JSONUtil.toJSON(msg));
        return JSONUtil.toJSON(msg);
    }

    @ResponseBody("/wx/findExpressByCode.do")
    public String findExpressByCode(HttpServletRequest request, HttpServletResponse response) {
        String code = request.getParameter("code");
        Express e = new ExpressServiceImpl().findByCode(code);
        Message msg = new Message();
        if (e != null) {
            BootstrapTableExpress bte = new BootstrapTableExpress();
            bte.setId(e.getId());
            bte.setCode(e.getCode());
            bte.setCompany(e.getCompany());
            bte.setInTime(DateFormatUtil.format(e.getInTime()));
            bte.setNumber(e.getNumber());
            msg.setStatus(0);
            msg.setResult("查询成功");
            msg.setData(bte);
        } else {
            msg.setStatus(-1);
            msg.setResult("查询失败");
        }
        return JSONUtil.toJSON(msg);
    }

    @ResponseBody("/wx/findExpressByNumber.do")
    public String findExpressByNumber(HttpServletRequest request, HttpServletResponse response) {
        String number = request.getParameter("number");
        Express e = new ExpressServiceImpl().findByNumber(number);
        Message msg = new Message();
        if (e != null) {
            BootstrapTableExpress bte = new BootstrapTableExpress();
            bte.setId(e.getId());
            bte.setCode(e.getCode()==null?"已取件":e.getCode());
            bte.setCompany(e.getCompany());
            bte.setInTime(DateFormatUtil.format(e.getInTime()));
            bte.setNumber(e.getNumber());
            msg.setStatus(0);
            msg.setResult("查询成功");
            msg.setData(bte);
        } else {
            msg.setStatus(-1);
            msg.setResult("查询失败");
        }
        System.out.println("QRCodeController->findExpressByNumber->json");
        System.out.println(JSONUtil.toJSON(msg));
        return JSONUtil.toJSON(msg);
    }

}
