package com.zcx.wx.Controller;

import com.zcx.bean.BootstrapTableExpress;
import com.zcx.bean.Express;
import com.zcx.bean.Message;
import com.zcx.mvc.ResponseBody;
import com.zcx.service.Impl.ExpressServiceImpl;
import com.zcx.util.DateFormatUtil;
import com.zcx.util.JSONUtil;
import com.zcx.util.UserUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExpressController {
    @ResponseBody("/wx/findExpressByUserPhone.do")
    public String findExpressByUserPhone(HttpServletRequest request, HttpServletResponse response) {
        String userPhone = UserUtil.getLoginPhone(request.getSession());
        //该用户所有的快递
        List<Express> sysList = new ExpressServiceImpl().findByUserPhone(userPhone);
        List<BootstrapTableExpress> status0Express = new ArrayList<>();
        List<BootstrapTableExpress> status1Express = new ArrayList<>();
        for (Express e : sysList) {
            String code = e.getCode() == null ? "已取件" : e.getCode();
            String inTime = DateFormatUtil.format(e.getInTime());
            String outTime = (e.getOutTime() == null) ? "未出库" : DateFormatUtil.format(e.getOutTime());
            String status = e.getStatus() == 0 ? "未取件" : "已取件";
            BootstrapTableExpress bte = new BootstrapTableExpress(e.getId(), e.getNumber(), e.getUsername(),
                    e.getUserPhone(), e.getCompany(), code, inTime, outTime, status, e.getSysPhone());
            if (e.getStatus() == 0) {
                status0Express.add(bte);
            } else {
                status1Express.add(bte);
            }
        }
//        Stream<BootstrapTableExpress> status0Express = pageList.stream().filter(express -> {
//            if (express.getStatus().equals("未取件")) {
//                return true;
//            } else {
//                return false;
//            }
//        }).sorted((o1, o2) -> {
//            long t1 = DateFormatUtil.parse(o1.getInTime());
//            long t2 = DateFormatUtil.parse(o2.getInTime());
//            return (int) (t1-t2);
//        });
//        Stream<BootstrapTableExpress> status1Express = pageList.stream().filter(express -> {
//            if (express.getStatus().equals("已取件")) {
//                return true;
//            } else {
//                return false;
//            }
//        }).sorted((o1, o2) -> {
//            long t1 = DateFormatUtil.parse(o1.getInTime());
//            long t2 = DateFormatUtil.parse(o2.getInTime());
//            return (int) (t1-t2);
//        });
        Map<String, List> data = new HashMap<>();
        data.put("status0", status0Express);
        data.put("status1", status1Express);
        Message msg = new Message();
        System.out.println("ExpressController->findExpressByUserPhone->userPhone:" + userPhone);
        msg.setResult(userPhone);
        msg.setData(data);
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseBody("/wx/userExpressList.do")
    public String userExpressList(HttpServletRequest request, HttpServletResponse response) {
        String userPhone = request.getParameter("userPhone");
        List<Express> sysList = new ExpressServiceImpl().findByUserPhone(userPhone);
        List<BootstrapTableExpress> status0Express = new ArrayList<>();
        for (Express e : sysList) {
            if (e.getStatus() == 0) {
                String code = e.getCode() == null ? "已取件" : e.getCode();
                String inTime = DateFormatUtil.format(e.getInTime());
                String outTime = (e.getOutTime() == null) ? "未出库" : DateFormatUtil.format(e.getOutTime());
                String status = e.getStatus() == 0 ? "未取件" : "已取件";
                BootstrapTableExpress bte = new BootstrapTableExpress(e.getId(), e.getNumber(), e.getUsername(),
                        e.getUserPhone(), e.getCompany(), code, inTime, outTime, status, e.getSysPhone());
                status0Express.add(bte);
            }
        }
        Message msg = new Message();
        if (status0Express.size() == 0) {
            msg.setStatus(-1);
            msg.setResult("未查询到快递");
        } else {
            msg.setStatus(0);
            msg.setData(status0Express);
        }
        String json = JSONUtil.toJSON(msg);
        System.out.println("ExpressController->/wx/userExpressList.do->json");
        System.out.println(json);
        return json;
    }

    @ResponseBody("/wx/expressInsert.do")
    public String insert(HttpServletRequest request, HttpServletResponse response) {
        String number = request.getParameter("number");
        String company = request.getParameter("company");
        String username = request.getParameter("username");
        String userPhone = request.getParameter("userPhone");
        String sysPhone = UserUtil.getLoginPhone(request.getSession());
        // TODO：还没编写柜子端，没有录入信息
        Express e = new Express(number, username, userPhone, company, sysPhone);
        System.out.println(e);
        boolean flag = new ExpressServiceImpl().insert(e);
        Message msg = new Message();
        if (flag) {
            msg.setStatus(0);
            msg.setResult("快递录入成功");
        } else {
            msg.setStatus(-1);
            msg.setResult("快递录入失败");
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseBody("/wx/lazyBoard.do")
    public String lazyBoard(HttpServletRequest request, HttpServletResponse response) {
        List<Map<String, String>> list = new ExpressServiceImpl().totalLazyBoard();
        Message msg = new Message();
        if (list.size() == 0) {
            msg.setStatus(-1);
            msg.setResult("数据查询失败，请刷新后重试！");
        }else {
            msg.setStatus(0);
            msg.setData(list);
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseBody("/wx/yearLazyBoard.do")
    public String yearLazyBoard(HttpServletRequest request, HttpServletResponse response) {
        List<Map<String, String>> list = new ExpressServiceImpl().yearLazyBoard();
        Message msg = new Message();
        if (list.size() == 0) {
            msg.setStatus(-1);
            msg.setResult("数据查询失败，请刷新后重试！");
        }else {
            msg.setStatus(0);
            msg.setData(list);
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseBody("/wx/monthLazyBoard.do")
    public String monthLazyBoard(HttpServletRequest request, HttpServletResponse response) {
        List<Map<String, String>> list = new ExpressServiceImpl().monthLazyBoard();
        Message msg = new Message();
        if (list.size() == 0) {
            msg.setStatus(-1);
            msg.setResult("数据查询失败，请刷新后重试！");
        }else {
            msg.setStatus(0);
            msg.setData(list);
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }
}
