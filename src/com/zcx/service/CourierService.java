package com.zcx.service;

import com.zcx.bean.Courier;

import java.util.List;
import java.util.Map;

public interface CourierService {
    /**
     * 用于查询数据库中的所有快递员信息
     * @return [size:快递员数， day:快递员日注册量]
     */
    Map<String, Integer> console();

    /**
     * 用于查询所有快递员信息
     * @param limit 是否分页，true分页，false查询所有
     * @param offset SQL语句中的起始索引
     * @param pageNumber 页查询的数量
     * @return 快递员的集合
     */
    List<Courier> findAll(boolean limit, int offset, int pageNumber);

    /**
     * 根据快递员手机号码查询快递员信息
     * @param phone 快递员手机号码
     * @return 查询快递员信息，快递员不存在时返回null
     */
    Courier findByPhone(String phone);

    /**
     * 添加快递员
     * @param courier 快递员对象
     * @return 录入是否成功，0表示成功，1表示手机号码重复，2表示身份证号码重复 -1表示其他错误
     */
    int insert(Courier courier);

    /**
     * 修改快递
     * @param id 要修改的快递员id
     * @param newCourier 新的快递员信息
     * @return 修改是否成功，0表示成功，1表示手机号码重复，2表示身份证号码重复 -1表示其他错误
     */
    int update(int id, Courier newCourier);

    /**
     * 删除快递员
     * @param id 要删除的快递员id
     * @return 删除是否成功，true表示成功，false表示失败
     */
    boolean delete(int id);
}
