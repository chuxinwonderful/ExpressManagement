package com.zcx.service.Impl;

import com.zcx.dao.Impl.AdminDaoImpl;
import com.zcx.service.AdminService;

import java.util.Date;

public class AdminServiceImpl implements AdminService {
    /**
     * 管理员根据账号密码登录
     * 判断是否登录成功
     *
     * @param userName 账号
     * @param password 密码
     * @return true 表示登录成功
     */
    public boolean login(String userName, String password) {
        return new AdminDaoImpl().login(userName, password);
    }

    /**
     * 通过admin的用户名
     * 来更新admin的登录时间和登录IP
     *
     * @param userName
     * @param date
     * @param ip
     */
    public void updateLoginTime(String userName, Date date, String ip) {
        new AdminDaoImpl().updateLoginTime(userName, date, ip);
    }
}
