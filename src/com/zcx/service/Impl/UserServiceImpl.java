package com.zcx.service.Impl;

import com.zcx.Exception.DuplicateIdNumberException;
import com.zcx.Exception.DuplicatePhoneException;
import com.zcx.bean.User;
import com.zcx.dao.Impl.UserDaoImpl;
import com.zcx.dao.UserDao;
import com.zcx.service.UserService;

import java.util.List;
import java.util.Map;

public class UserServiceImpl implements UserService {

    UserDao userDao = new UserDaoImpl();
    /**
     * 用于查询数据库中的所有用户信息
     *
     * @return [size:用户数， day:日注册量]
     */
    @Override
    public Map<String, Integer> console() {
        return userDao.console();
    }

    /**
     * 用于查询所有用户信息
     *
     * @param limit      是否分页，true分页，false查询所有
     * @param offset     SQL语句中的起始索引
     * @param pageNumber 页查询的数量
     * @return 用户的集合
     */
    @Override
    public List<User> findAll(boolean limit, int offset, int pageNumber) {
        return userDao.findAll(limit, offset, pageNumber);
    }

    /**
     * 根据用户手机号码查询用户信息
     * @param phone 用户手机号码
     * @return 查询用户信息，用户不存在时返回null
     */
    @Override
    public User findByPhone(String phone) {
        return userDao.findByPhone(phone);
    }

    /**
     * 根据用户ID查询快递
     * @param id 编号
     * @return 查询用户信息，用户不存在时返回null
     */
    @Override
    public User findById(int id) {
        return userDao.findById(id);
    }

    /**
     * 添加用户
     *
     * @param user 用户对象
     * @return 录入是否成功，0表示成功，1表示手机号码重复，2表示身份证号码重复 -1表示其他错误
     */
    @Override
    public int insert(User user) {
        int flag = -1;
        try {
            if (userDao.insert(user)) {
                flag = 0;
            }
        } catch (DuplicatePhoneException e) {
            flag = 1;
        } catch (DuplicateIdNumberException e) {
            flag = 2;
        }
        return flag;
    }

    /**
     * 修改快递
     * @param id 要修改的用户id
     * @param newUser 新的用户信息
     * @return 修改是否成功，0表示成功，1表示手机号码重复，2表示身份证号码重复 -1表示其他错误
     */
    @Override
    public int update(int id, User newUser) {
        int flag = -1;
        try {
            if(userDao.update(id,newUser)){
                flag = 0;
            }
        } catch (DuplicatePhoneException e) {
            flag = 1;
        } catch (DuplicateIdNumberException e) {
            flag = 2;
        }
        return flag;
    }

    /**
     * 删除用户
     * @param id 要删除的用户id
     * @return 删除是否成功，true表示成功，false表示失败
     */
    @Override
    public boolean delete(int id) {
        return userDao.delete(id);
    }
}
