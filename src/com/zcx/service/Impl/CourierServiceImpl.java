package com.zcx.service.Impl;

import com.zcx.Exception.DuplicateIdNumberException;
import com.zcx.Exception.DuplicatePhoneException;
import com.zcx.bean.Courier;
import com.zcx.dao.CourierDao;
import com.zcx.dao.Impl.CourierDaoImpl;
import com.zcx.service.CourierService;

import java.util.List;
import java.util.Map;

public class CourierServiceImpl implements CourierService {
    CourierDao courierDao = new CourierDaoImpl();
    /**
     * 用于查询数据库中的所有快递员信息
     * @return [size:快递员数， day:快递员日注册量]
     */
    @Override
    public Map<String, Integer> console() {
        return courierDao.console();
    }

    /**
     * 用于查询所有快递员信息
     * @param limit      是否分页，true分页，false查询所有
     * @param offset     SQL语句中的起始索引
     * @param pageNumber 页查询的数量
     * @return 快递员的集合
     */
    @Override
    public List<Courier> findAll(boolean limit, int offset, int pageNumber) {
        return courierDao.findAll(limit, offset, pageNumber);
    }

    /**
     * 根据快递员手机号码查询快递员信息
     * @param phone 快递员手机号码
     * @return 查询快递员信息，快递员不存在时返回null
     */
    @Override
    public Courier findByPhone(String phone) {
        return courierDao.findByPhone(phone);
    }

    /**
     * 添加快递员
     * @param courier 快递员对象
     * @return 录入是否成功，0表示成功，1表示手机号码重复，2表示身份证号码重复 -1表示其他错误
     */
    @Override
    public int insert(Courier courier) {
        int flag = -1;
        try {
            if(courierDao.insert(courier)){
                flag = 0;
            }
        } catch (DuplicatePhoneException e) {
            flag = 1;
        } catch (DuplicateIdNumberException e) {
            flag = 2;
        }
        return flag;
    }

    /**
     * 修改快递
     *
     * @param id         要修改的快递员id
     * @param newCourier 新的快递员信息
     * @return 修改是否成功，0表示成功，1表示手机号码重复，2表示身份证号码重复 -1表示其他错误
     */
    @Override
    public int update(int id, Courier newCourier) {
        int flag = -1;
        try {
            if(courierDao.update(id,newCourier)){
                flag = 0;
            }
        } catch (DuplicatePhoneException e) {
            flag = 1;
        } catch (DuplicateIdNumberException e) {
            flag = 2;
        }
        return flag;
    }

    /**
     * 删除快递员
     *
     * @param id 要删除的快递员id
     * @return 删除是否成功，true表示成功，false表示失败
     */
    @Override
    public boolean delete(int id) {
        return courierDao.delete(id);
    }
}
