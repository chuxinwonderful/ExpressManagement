package com.zcx.service.Impl;

import com.zcx.Exception.DuplicateCodeException;
import com.zcx.bean.Express;
import com.zcx.dao.ExpressDao;
import com.zcx.dao.Impl.ExpressDaoImpl;
import com.zcx.service.ExpressService;
import com.zcx.util.RandomUtil;

import java.util.List;
import java.util.Map;

public class ExpressServiceImpl implements ExpressService {
    private ExpressDao dao = new ExpressDaoImpl();
    /**
     * 用于查询数据库中的所有快递（含总数和今日新增数），
     * 待取快递（含总数和今日数）
     *
     * @return [
     * {size:总数， day:新增},//所有
     * {size:总数， day:新增}//待取
     * ]
     */
    @Override
    public List<Map<String, Integer>> console() {
        return dao.console();
    }

    /**
     * 用于查询所有快递
     *
     * @param limit      是否分页，true分页，false查询所有
     * @param offset     SQL语句中的起始索引
     * @param pageNumber 页查询的数量
     * @return 快递的集合
     */
    @Override
    public List<Express> findAll(boolean limit, int offset, int pageNumber) {
        return dao.findAll(limit, offset, pageNumber);
    }

    /**
     * 根据快递单号查询快递
     *
     * @param number 单号
     * @return 查询的快递信息，快递不存在时返回null
     */
    @Override
    public Express findByNumber(String number) {
        return dao.findByNumber(number);
    }

    /**
     * 根据取件码查询快递
     *
     * @param code 取件码
     * @return 查询的快递信息，快递不存在时返回null
     */
    @Override
    public Express findByCode(String code) {
        return dao.findByCode(code);
    }

    /**
     * 根据用户手机号查询他所有的快递
     *
     * @param userPhone 用户手机号
     * @return 快递的集合
     */
    @Override
    public List<Express> findByUserPhone(String userPhone) {
        return dao.findByUserPhone(userPhone);
    }

    /**
     * 根据录入人的手机号码查询录入的所有记录
     *
     * @param sysPhone 录入人的手机号码
     * @return 快递的集合
     */
    @Override
    public List<Express> findBySysPhone(String sysPhone) {
        return dao.findBySysPhone(sysPhone);
    }

    /**
     * 录入快递
     * @param e 快递对象
     * @return 录入是否成功，true表示成功，false表示失败
     */
    @Override
    public boolean insert(Express e) {
        e.setCode(RandomUtil.getCode()+"");
        try {
            return dao.insert(e);
        } catch (DuplicateCodeException duplicateCodeException) {
            return insert(e);
        }
    }

    /**
     * 修改快递
     *
     * @param id 要修改的快递id
     * @param newExpress 新的快递的信息（number,company,username,userPhone,status）
     * @return 修改是否成功，true表示成功，false表示失败
     */
    @Override
    public boolean update(int id, Express newExpress) {
        boolean update = false;
        Express oldExpress =  dao.findById(id);
        //需要修改收件人的手机号码时
        if (newExpress.getUserPhone() != null && oldExpress.getUsername() == newExpress.getUsername()){
            boolean delete = dao.delete(id);
            boolean insert = insert(newExpress);
            update = delete && insert;
        }else {
            update = dao.update(id, newExpress);
            //对象状态从未取件变成已取件时
            if (oldExpress.getStatus() == 0 && newExpress.getStatus() == 1){
                update = updateStatus(oldExpress.getCode());
            }
        }
        return update;
    }

    /**
     * 确认取件，修改快递状态
     *
     * @param code 要修改的快递取件码
     * @return 修改状态是否成功，true表示成功，false表示失败
     */
    @Override
    public boolean updateStatus(String code) {
        return dao.updateStatus(code);
    }

    /**
     * 删除快递
     *
     * @param id 要删除的快递id
     * @return 删除是否成功，true表示成功，false表示失败
     */
    @Override
    public boolean delete(int id) {
        return dao.delete(id);
    }

    public List<Map<String, String>> totalLazyBoard() {
        return dao.totalLazyBoard();
    }
    public List<Map<String, String>> yearLazyBoard() {
        return dao.yearLazyBoard();
    }
    public List<Map<String, String>> monthLazyBoard() {
        return dao.monthLazyBoard();
    }

}

