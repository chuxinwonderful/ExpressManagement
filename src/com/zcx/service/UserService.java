package com.zcx.service;

import com.zcx.bean.User;

import java.util.List;
import java.util.Map;

public interface UserService {
    /**
     * 用于查询数据库中的所有用户信息
     * @return [size:用户数， day:日注册量]
     */
    Map<String, Integer> console();

    /**
     * 用于查询所有用户信息
     * @param limit 是否分页，true分页，false查询所有
     * @param offset SQL语句中的起始索引
     * @param pageNumber 页查询的数量
     * @return 用户的集合
     */
    List<User> findAll(boolean limit, int offset, int pageNumber);

    /**
     * 根据用户手机号码查询用户信息
     * @param phone 用户手机号码
     * @return 查询用户信息，用户不存在时返回null
     */
    User findByPhone(String phone);

    /**
     * 根据用户ID查询快递
     * @param id 编号
     * @return 查询用户信息，用户不存在时返回null
     */
    User findById(int id);

    /**
     * 添加用户
     * @param user 用户对象
     * @return 录入是否成功，0表示成功，1表示手机号码重复，2表示身份证号码重复 -1表示其他错误
     */
    int insert(User user);

    /**
     * 修改快递
     * @param id 要修改的用户id
     * @param newUser 新的用户信息
     * @return 修改是否成功，0表示成功，1表示手机号码重复，2表示身份证号码重复 -1表示其他错误
     */
    int update(int id, User newUser);

    /**
     * 删除用户
     * @param id 要删除的用户id
     * @return 删除是否成功，true表示成功，false表示失败
     */
    boolean delete(int id);
}
