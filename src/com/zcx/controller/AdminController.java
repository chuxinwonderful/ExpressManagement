package com.zcx.controller;

import com.zcx.bean.Message;
import com.zcx.mvc.ResponseBody;
import com.zcx.mvc.ResponseView;
import com.zcx.service.AdminService;
import com.zcx.service.Impl.AdminServiceImpl;
import com.zcx.util.JSONUtil;
import com.zcx.util.UserUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

public class AdminController {

    @ResponseBody("/admin/login.do")
    public String login(HttpServletRequest req, HttpServletResponse resp) {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        AdminService adminService = new AdminServiceImpl();
        boolean result = adminService.login(username, password);
        Message msg = null;
        if (result) {
            msg = new Message(0, "登录成功");
            Date date = new Date();
            String ip = req.getRemoteAddr();
            adminService.updateLoginTime(username, date, ip);
            req.getSession().setAttribute("adminUserName", username);
        } else {
            msg = new Message(-1, "登录失败");
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseBody("/admin/index.do")
    public String index(HttpServletRequest req, HttpServletResponse resp) {
        String adminName = UserUtil.getUserName(req.getSession());
        Message msg = new Message();
        if (adminName != null){
            msg.setStatus(0);
            msg.setResult(adminName);
        }else{
            msg.setStatus(-1);
            msg.setResult("");
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseView("/admin/loginOut.do")
    public String loginOut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.getSession().invalidate();
        String  path = "/admin/login.html";
        return path;
    }
}
