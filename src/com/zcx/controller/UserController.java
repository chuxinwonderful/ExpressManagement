package com.zcx.controller;

import com.zcx.bean.*;
import com.zcx.mvc.ResponseBody;
import com.zcx.service.Impl.UserServiceImpl;
import com.zcx.service.UserService;
import com.zcx.util.DateFormatUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.zcx.util.JSONUtil.toJSON;

public class UserController {
    UserService userService = new UserServiceImpl();

    @ResponseBody("/user/console.do")
    public String console(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Integer> data = userService.console();
        Message msg = new Message();
        if (data.size() != 0) {
            msg.setStatus(0);
            msg.setData(data);
        } else {
            msg.setStatus(-1);
        }
        String json = toJSON(msg);
        return json;
    }

    @ResponseBody("/user/list.do")
    public String list(HttpServletRequest request, HttpServletResponse response) {
        //获取查询数据的起始索引值
        int offset = Integer.parseInt(request.getParameter("offset"));
        //获取当前页面查询的数据量
        int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
        //后端查到是当前页数据
        List<User> serverData = userService.findAll(true, offset, pageNumber);
        //前端看到的当前页数据
        List<BootstrapTableUser> pageData = new ArrayList<>();
        for (User user : serverData) {
            int id = user.getId();
            String nickname = user.getNickname();
            String phone = user.getPhone();
            String password = user.getPassword();
            String registerTime = DateFormatUtil.format(user.getRegisterTime());
            String lastLoginTime = (user.getLastLoginTime() == null) ?
                    "---" : DateFormatUtil.format(user.getLastLoginTime());
            BootstrapTableUser btUser = new BootstrapTableUser(id, nickname, phone, password, registerTime, lastLoginTime);
            pageData.add(btUser);
        }
        Map<String, Integer> console = userService.console();
        Integer total = console.get("size");
        ResultData<BootstrapTableUser> resultData = new ResultData<>();
        resultData.setTotal(total);
        resultData.setRows(pageData);
        String json = toJSON(resultData);
        return json;
    }

    @ResponseBody("/user/insert.do")
    public String insert(HttpServletRequest request, HttpServletResponse response) {
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String idNumber = request.getParameter("idNumber");
        String password = request.getParameter("password");
        User user = new User(name, phone, idNumber, password);
        //flag: 0表示成功，1表示手机号码重复，2表示身份证号码重复 -1表示其他错误
        int flag = userService.insert(user);
        Message msg = new Message();
        if (flag == 0) {
            msg.setStatus(0);
            msg.setResult("添加成功");
        } else {
            msg.setStatus(-1);
            if (flag == 1) {
                msg.setResult("插入失败，手机号码已存在");
            } else if (flag == 2) {
                msg.setResult("插入失败，身份证号码已存在");
            } else {
                msg.setResult("插入失败");
            }
        }
        String json = toJSON(msg);
        return json;
    }

    @ResponseBody("/user/find.do")
    public String find(HttpServletRequest request, HttpServletResponse response) {
        String phone = request.getParameter("phone");
        User user = userService.findByPhone(phone);
        Message msg = new Message();
        if (user != null) {
            msg.setStatus(0);
            msg.setResult("查找成功");
            msg.setData(user);
        } else {
            msg.setStatus(-1);
            msg.setResult("查找失败");
        }
        String json = toJSON(msg);
        return json;
    }

    @ResponseBody("/user/update.do")
    public String update(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String idNumber = request.getParameter("idNumber");
        String password = request.getParameter("password");
        User user = new User(name, phone, idNumber, password);
        //flag: 0表示成功，1表示手机号码重复，2表示身份证号码重复 -1表示其他错误
        Message msg = new Message();
        int flag = userService.update(id, user);
        if (flag == 0) {
            msg.setStatus(0);
            msg.setResult("修改成功");
        } else {
            msg.setStatus(-1);
            if (flag == 1) {
                msg.setResult("修改失败，手机号码已存在");
            } else if (flag == 2) {
                msg.setResult("修改失败，身份证号码已存在");
            } else {
                msg.setResult("修改失败");
            }

        }
        String json = toJSON(msg);
        return json;
    }

    @ResponseBody("/user/delete.do")
    public String delete(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        boolean flag = userService.delete(id);
        Message msg = new Message();
        if (flag) {
            msg.setStatus(0);
            msg.setResult("删除成功");
        } else {
            msg.setStatus(-1);
            msg.setResult("删除失败");
        }
        String json = toJSON(msg);
        return json;
    }

}
