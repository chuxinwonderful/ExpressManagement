package com.zcx.controller;

import com.zcx.bean.BootstrapTableExpress;
import com.zcx.bean.Express;
import com.zcx.bean.Message;
import com.zcx.bean.ResultData;
import com.zcx.mvc.ResponseBody;
import com.zcx.service.Impl.ExpressServiceImpl;
import com.zcx.util.DateFormatUtil;
import com.zcx.util.JSONUtil;
import com.zcx.util.UserUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExpressController {

    ExpressServiceImpl expressService = new ExpressServiceImpl();

    @ResponseBody("/express/console.do")
    public String console(HttpServletRequest request, HttpServletResponse response){
        List<Map<String, Integer>> data = expressService.console();
        Message msg = new Message();
        if(data.size() == 0){
            msg.setStatus(-1);
        }else {
            msg.setStatus(0);
        }
        msg.setData(data);
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseBody("/express/list.do")
    public String list(HttpServletRequest request, HttpServletResponse response){
        //获取查询数据的起始索引值
        int offset = Integer.parseInt(request.getParameter("offset"));
        //获取当前页面查询的数据量
        int pageNumber = Integer.parseInt(request.getParameter( "pageNumber"));
        //开始查询
        //后端查到的快递对象集合
        List<Express> serverList = expressService.findAll(true, offset, pageNumber);
        //在前端显示的快递集合对象（用户看到的更直观些）
        List<BootstrapTableExpress> pageList = new ArrayList<>();
        for (Express e : serverList){
            String code = e.getCode()==null ? "已取件" : e.getCode();
            String inTime = DateFormatUtil.format(e.getInTime());
            String outTime = (e.getOutTime() == null) ? "未出库" : DateFormatUtil.format(e.getOutTime());
            String status = e.getStatus()==0 ? "未取件" : "已取件";
            BootstrapTableExpress bte = new BootstrapTableExpress(e.getId(), e.getNumber(), e.getUsername(),
                    e.getUserPhone(), e.getCompany(), code, inTime, outTime, status, e.getSysPhone());
            pageList.add(bte);
        }
        List<Map<String, Integer>> console = expressService.console();
        //总数量
        Integer total = console.get(0).get("size");
        ResultData<BootstrapTableExpress> resultData = new ResultData<>();
        resultData.setRows(pageList);
        resultData.setTotal(total);
        String json = JSONUtil.toJSON(resultData);
        return json;
    }

    @ResponseBody("/express/insert.do")
    public String insert(HttpServletRequest request, HttpServletResponse response){
        String number = request.getParameter("number");
        String company = request.getParameter("company");
        String username = request.getParameter("username");
        String userPhone = request.getParameter("userPhone");
        // TODO：还没编写柜子端，没有录入信息
        Express e = new Express(number, username, userPhone, company, UserUtil.getUserPhone(request.getSession()));
        boolean flag = expressService.insert(e);
        Message msg = new Message();
        if (flag){
            msg.setStatus(0);
            msg.setResult("快递录入成功");
        }else{
            msg.setStatus(-1);
            msg.setResult("快递录入失败");
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseBody("/express/find.do")
    public String find(HttpServletRequest request, HttpServletResponse response){
        String number = request.getParameter("number");
        Express e = expressService.findByNumber(number);
        Message msg = new Message();
        if (e == null){
            msg.setStatus(-1);
            msg.setResult("单号不存在");
        }else{
            msg.setStatus(0);
            msg.setResult("查询成功");
            msg.setData(e);
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseBody("/express/update.do")
    public String update(HttpServletRequest request, HttpServletResponse response){
        int id = Integer.parseInt(request.getParameter("id"));
        String number = request.getParameter("number");
        String company = request.getParameter("company");
        String username = request.getParameter("username");
        String userPhone = request.getParameter("userPhone");
        int status = Integer.parseInt(request.getParameter("status"));
        Express newExpress = new Express();
        newExpress.setNumber(number);
        newExpress.setCompany(company);
        newExpress.setUsername(username);
        newExpress.setUserPhone(userPhone);
        newExpress.setStatus(status);
        boolean flag = expressService.update(id, newExpress);
        Message msg = new Message();
        if (flag){
            msg.setStatus(0);
            msg.setResult("修改成功");
        }else {
            msg.setStatus(-1);
            msg.setResult("修改失败");
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseBody("/express/delete.do")
    public String delete(HttpServletRequest request, HttpServletResponse response){
        int id = Integer.parseInt(request.getParameter("id"));
        boolean flag = expressService.delete(id);
        System.out.println("delete.do id:"+id);
        Message msg = new Message();
        if (flag){
            msg.setStatus(0);
            msg.setResult("删除成功");
        }else {
            msg.setStatus(-1);
            msg.setResult("删除失败");
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }
}
