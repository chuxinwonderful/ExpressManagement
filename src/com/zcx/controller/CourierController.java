package com.zcx.controller;

import com.zcx.bean.BootstrapTableCourier;
import com.zcx.bean.Courier;
import com.zcx.bean.Message;
import com.zcx.bean.ResultData;
import com.zcx.mvc.ResponseBody;
import com.zcx.service.CourierService;
import com.zcx.service.Impl.CourierServiceImpl;
import com.zcx.util.DateFormatUtil;
import com.zcx.util.JSONUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CourierController {
    CourierService courierService = new CourierServiceImpl();

    @ResponseBody("/courier/console.do")
    public String console(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Integer> data = courierService.console();
        Message msg = new Message();
        if (data.size() == 0) {
            msg.setStatus(-1);
        } else {
            msg.setStatus(0);
            msg.setData(data);
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseBody("/courier/list.do")
    public String list(HttpServletRequest request, HttpServletResponse response) {
        int offset = Integer.parseInt(request.getParameter("offset"));
        int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
        List<Courier> serverData = courierService.findAll(true, offset, pageNumber);
        List<BootstrapTableCourier> pageData = new ArrayList<>();
        for (Courier courier : serverData) {
            int id = courier.getId();
            String name = courier.getName();
            String phone = courier.getPhone();
            String idNumber = courier.getIdNumber();
            String password = courier.getPassword();
            int sendNumber = courier.getSendNumber();
            String registerTime = DateFormatUtil.format(courier.getRegisterTime());
            String lastLoginTime = courier.getLastLoginTime() == null ?
                    "----" :
                    DateFormatUtil.format(courier.getLastLoginTime());
            BootstrapTableCourier btCourier = new BootstrapTableCourier(id, name, phone, idNumber, password,
                    sendNumber, registerTime, lastLoginTime);
            pageData.add(btCourier);
        }
        Integer total = courierService.console().get("size");
        ResultData<BootstrapTableCourier> resultData = new ResultData<>();
        resultData.setTotal(total);
        resultData.setRows(pageData);
        String json = JSONUtil.toJSON(resultData);
        return json;
    }

    @ResponseBody("/courier/insert.do")
    public String insert(HttpServletRequest request, HttpServletResponse response) {
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String idNumber = request.getParameter("idNumber");
        String password = request.getParameter("password");
        Courier courier = new Courier(name, phone, idNumber, password);
        //录入是否成功，0表示成功，1表示手机号码重复，2表示身份证号码重复 -1表示其他错误
        int flag = courierService.insert(courier);
        Message msg = new Message();
        if (flag == 0) {
            msg.setStatus(0);
            msg.setResult("添加成功");
        } else {
            msg.setStatus(-1);
            if (flag == 1) {
                msg.setResult("添加失败，手机号码已存在");
            } else if (flag == 2) {
                msg.setResult("添加失败，身份证号码已存在");
            } else {
                msg.setResult("添加失败");
            }
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseBody("/courier/find.do")
    public String find(HttpServletRequest request, HttpServletResponse response) {
        String phone = request.getParameter("phone");
        Courier courier = courierService.findByPhone(phone);
        Message msg = new Message();
        if (courier != null) {
            msg.setStatus(0);
            msg.setResult("查找成功");
            msg.setData(courier);
        } else {
            msg.setStatus(-1);
            msg.setResult("查找失败");
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseBody("/courier/delete.do")
    public String delete(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        boolean flag = courierService.delete(id);
        Message msg = new Message();
        if (flag) {
            msg.setStatus(0);
            msg.setResult("删除成功");
        } else {
            msg.setStatus(-1);
            msg.setResult("删除失败");
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }

    @ResponseBody("/courier/update.do")
    public String update(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String idNumber = request.getParameter("idNumber");
        String password = request.getParameter("password");
        Courier courier = new Courier(name, phone, idNumber, password);
        //更改是否成功，0表示成功，1表示手机号码重复，2表示身份证号码重复 -1表示其他错误
        int flag = courierService.update(id, courier);
        Message msg = new Message();
        if (flag == 0) {
            msg.setStatus(0);
            msg.setResult("修改成功");
        } else {
            msg.setStatus(-1);
            if (flag == 1) {
                msg.setResult("修改失败，手机号码已存在");
            } else if (flag == 2) {
                msg.setResult("修改失败，身份证号码已存在");
            } else {
                msg.setResult("修改失败");
            }
        }
        String json = JSONUtil.toJSON(msg);
        return json;
    }
}
