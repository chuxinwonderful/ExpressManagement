package com.zcx.util;

import org.junit.Test;

import java.sql.Connection;

public class DruidUtilTest {

    @Test
    public void getConnection() {
        DruidUtil.getConnection();
    }

    @Test
    public void close() {
        Connection conn = DruidUtil.getConnection();
        DruidUtil.close(conn,null,null);
    }
}