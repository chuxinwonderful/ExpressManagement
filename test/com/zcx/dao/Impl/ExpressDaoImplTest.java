package com.zcx.dao.Impl;

import com.zcx.Exception.DuplicateCodeException;
import com.zcx.bean.Express;
import com.zcx.service.Impl.ExpressServiceImpl;
import org.junit.Test;

import java.util.List;
import java.util.Map;

public class ExpressDaoImplTest {
    ExpressDaoImpl expressDao = new ExpressDaoImpl();

    @Test
    public void console() {
        List<Map<String, Integer>> console = expressDao.console();
        System.out.println(console);
    }

    @Test
    public void findAll() {
        List<Express> all = expressDao.findAll(false, 0, 0);
        System.out.println(all);
        System.out.println("-----------------------");
        List<Express> limit1 = expressDao.findAll(true, 0, 2);
        System.out.println(limit1);
        System.out.println("-----------------------");
        List<Express> limit2 = expressDao.findAll(true, 2, 2);
        System.out.println(limit2);
    }

    @Test
    public void findByNumber() {
        Express byNumber = expressDao.findByNumber("123457");
        System.out.println(byNumber);
    }

    @Test
    public void findById() {
        Express byId = expressDao.findById(1);
        System.out.println(byId);
    }

    @Test
    public void findByCode() {
        Express byCode = expressDao.findByCode("654323");
        System.out.println(byCode);
    }

    @Test
    public void findByUserPhone() {
        List<Express> byUserPhone = expressDao.findByUserPhone("12345678910");
        System.out.println(byUserPhone);
    }

    @Test
    public void findBySysPhone() {
        List<Express> bySysPhone = expressDao.findBySysPhone("12345678900");
        System.out.println(bySysPhone);
    }

    @Test
    public void insert() {
        Express e = new Express("123411", "黄二", "12345678909"
                , "中通", "12345678912", "666666");
        boolean insert = false;
        try {
            insert = expressDao.insert(e);
        } catch (DuplicateCodeException duplicateCodeException) {
            System.out.println("取件码重复");
        }
        System.out.println(insert);
    }

    @Test
    public void insertData() {
        for (int i = 1; i < 100; i++) {
            Express e = new Express("23456" + i, "黄二", "12345678909"
                    , "中通", "12345678912", "666666");

            boolean insert = false;
            insert = new ExpressServiceImpl().insert(e);
            System.out.println(insert);
        }
    }

    @Test
    public void update() {
        Express e = new Express();
        expressDao.update(1, e);
    }

    @Test
    public void updateStatus() {
        expressDao.updateStatus("");
    }

    @Test
    public void delete() {
        System.out.println(expressDao.delete(208));
    }

    @Test
    public void totalLazyBoard() {
        System.out.println(expressDao.totalLazyBoard());
    }

    @Test
    public void yearLazyBoard() {
        System.out.println(expressDao.yearLazyBoard());
    }

    @Test
    public void monthLazyBoard() {
        System.out.println(expressDao.monthLazyBoard());
    }
}