package com.zcx.dao.Impl;

import com.zcx.Exception.DuplicateIdNumberException;
import com.zcx.Exception.DuplicatePhoneException;
import com.zcx.bean.Courier;
import com.zcx.dao.CourierDao;
import org.junit.Test;

public class CourierDaoImplTest {
    CourierDao courierDao = new CourierDaoImpl();
    @Test
    public void console() {
        System.out.println(courierDao.console());
    }

    @Test
    public void findAll() {
        System.out.println(courierDao.findAll(true,0,2));
        System.out.println("=============");
        System.out.println(courierDao.findAll(false,0,0));
    }

    @Test
    public void findByPhone() {
        System.out.println(courierDao.findByPhone("15365498810"));
    }

    @Test
    public void insert() throws DuplicateIdNumberException, DuplicatePhoneException {
        Courier courier = new Courier("郑四","12365498704","987654321987654318","zhengsi");
        System.out.println(courierDao.insert(courier));
    }

    @Test
    public void insertData() throws DuplicateIdNumberException, DuplicatePhoneException {
        int phone = 710;
        int idNum = 310;
        for (int i=0;i<100;i++) {
            Courier courier = new Courier("郑测试", "12365498"+(++phone),
                    "987654321987654"+(--idNum), "zhengsi");
            System.out.println(courierDao.insert(courier));
        }
    }

    @Test
    public void update() throws DuplicateIdNumberException, DuplicatePhoneException {
        Courier newCourier = new Courier("郑四","12365498543","987654321987654318","zhengsi");
        System.out.println(courierDao.update(22,newCourier));
    }

    @Test
    public void delete() {
        System.out.println(courierDao.delete(21));
    }
}