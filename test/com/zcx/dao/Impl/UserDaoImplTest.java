package com.zcx.dao.Impl;

import com.zcx.Exception.DuplicateIdNumberException;
import com.zcx.Exception.DuplicatePhoneException;
import com.zcx.bean.User;
import com.zcx.dao.UserDao;
import org.junit.Test;

public class UserDaoImplTest {
    @Test
    public void test() {
        User user = new User("123");
        System.out.println(user);
    }
    UserDao userDao = new UserDaoImpl();
    @Test
    public void console() {
        System.out.println(userDao.console());
    }

    @Test
    public void findAll() {
        System.out.println(userDao.findAll(true,0,2));
    }

    @Test
    public void findByPhone() {
        System.out.println(userDao.findByPhone("13602664849"));
    }

    @Test
    public void findById() {
        System.out.println(userDao.findById(4));
    }

    @Test
    public void insert() throws DuplicateIdNumberException, DuplicatePhoneException {
        User user = new User("黄三","12345678910","123456789098765321","huangsan");
        System.out.println(userDao.insert(user));
    }
    @Test
    public void insertMany() throws DuplicateIdNumberException, DuplicatePhoneException {
        int phone =78910;
        int idNum = 8765321;
        for (int i=0; i<100; i++){
            User user = new User("黄测试","123456"+(++phone),"12345678909"+(++idNum),"123");
            System.out.println(userDao.insert(user));
        }
    }
    @Test
    public void update() throws DuplicateIdNumberException, DuplicatePhoneException {
        User newUser = new User("黄一","12345678908","123456789098765320","huangyi");
        System.out.println(userDao.update(1,newUser));
    }

    @Test
    public void delete() {
        userDao.delete(5);
    }
}